//
//  SKYTests.swift
//  SKYTests
//
//  Created by marcos hayashi on 25/07/18.
//  Copyright © 2018 marcos hayashi. All rights reserved.
//

import XCTest
@testable import SKY

class SKYTests: XCTestCase {
    
    
    var movies:[Movie] = []
    
    
    override func setUp() {
        super.setUp()
        
        movies = []
        
       
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test01() {
         let expectation = XCTestExpectation(description: "Recuperando dados do servidor")
        
        MovieManage.getMovies(onComplete: { (listMovies) in
            XCTAssertNotNil(listMovies)
            
            expectation.fulfill()
        }) { (erro) in
            XCTAssert(false)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
        
    }
    
    
  
    
   
    
}
