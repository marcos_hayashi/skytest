//
//  ViewController.swift
//  SKY
//
//  Created by marcos hayashi on 25/07/18.
//  Copyright © 2018 marcos hayashi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //MARK: OUTLET
    @IBOutlet weak var cvFilmList: UICollectionView!
    
    var movies:[Movie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationItem.title = "CINE SKY"
        //navigationController?.setNavigationBarHidden(true, animated: true)
        MovieManage.getMovies(onComplete: { (movies) in
            self.movies = movies
           
            DispatchQueue.main.async {
                self.cvFilmList.reloadData()
            }
        }) { (erro) in
            switch erro{
            case    .URL_ERROR:
                print("tratar erro de URL")
            case    .TASK_ERROR:
                print("tratar erro de execucao da task")
            case    .NO_RESPONDE:
                print("tratar sem responde")
            case    .NO_DATA:
                print("tratar erro sen Data")
            case    .RESPONSE_STATUS_CODE:
                print("tratar erro por codigo do servidor")
            case    .INVALID_JSON:
                print("tratar erro de json")
            }
        }
        
    }
    @IBAction func openAnotherStoryBoard(_ sender: Any) {
//        let storyBoard = UIStoryboard(name: "SecondStory", bundle: nil)
//        let mainViewController = storyBoard.instantiateViewController(withIdentifier: "inicial")
//        self.navigationController?.pushViewController(mainViewController, animated: true)
        let vc = testeViewController(nibName: "testeViewController", bundle: nil)
        self.navigationController!.pushViewController(vc, animated: true)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }


}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCollectionViewCell", for: indexPath) as! MovieCollectionViewCell
        cell.prepare(movie: movies[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "collectionHeaderView", for: indexPath) 
            
            headerView.backgroundColor = UIColor.black
            return headerView
            
        default:
            
            assert(false, "Unexpected element kind")
        }
    
    }
    
}


