//
//  MovieCollectionViewCell.swift
//  SKY
//
//  Created by marcos hayashi on 25/07/18.
//  Copyright © 2018 marcos hayashi. All rights reserved.
//

import UIKit
import Kingfisher
class MovieCollectionViewCell: UICollectionViewCell {
    // MARK: OUTLET CELL
    @IBOutlet weak var ivMovie: UIImageView!
    @IBOutlet weak var lbTitleMovie: UILabel!
    
    
    // MARK: PREPARE CELL
    func prepare(movie:Movie){
        
        lbTitleMovie.text = movie.title
        
        
        
        //Recuperando image
        let imageUrl:URL = URL(string: movie.cover_url)!
        
        //image default
        let image = UIImage(named: "logo")
        
        //loading
        ivMovie.kf.indicatorType = .activity
        
        ivMovie.kf.setImage(with: imageUrl, placeholder:image)
        //ivMovie.kf.setImage(with: imageUrl)
//            URLSession.shared.dataTask(with: imageUrl, completionHandler: { (data, response, error) in
//
//                if error != nil {
//                    print(error!)
//                    return
//                }
//
//                DispatchQueue.main.async {
//                    self.ivMovie.image = UIImage(data: data!)
//                }
//            }).resume()
        
    }
    
}
