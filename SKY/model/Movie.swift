//
//  Movie.swift
//  SKY
//
//  Created by marcos hayashi on 25/07/18.
//  Copyright © 2018 marcos hayashi. All rights reserved.
//

import Foundation

class Movie:Codable {
    
    var title:String!
    var overview:String!
    var duration:String!
    var release_year:String!
    var cover_url:String!
    var backdrops_url:[String]!
    var id:String!
    
    init(title:String, overview:String, duration:String, release_year:String, cover_url:String, backdrops_url:[String], id:String ) {
        self.title = title
        self.overview =  overview
        self.duration = duration
        self.release_year = release_year
        self.cover_url = cover_url
        self.backdrops_url = backdrops_url
        self.id = id
    }
    
    
}

