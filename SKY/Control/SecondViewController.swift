//
//  SecondViewController.swift
//  SKY
//
//  Created by Marcos Hayashi Ikegami on 28/05/19.
//  Copyright © 2019 marcos hayashi. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var tfCPF: UITextField!
    @IBOutlet weak var tfCNPJ: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfCPF.delegate = self
        tfCNPJ.delegate = self
        
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension SecondViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        

        if textField.tag == 1 {
            
            if(range.location == 3 && range.length == 0){
                textField.text = textField.text! + "."
            }
            if(range.location == 7 && range.length == 0){
                textField.text = textField.text! + "."
            }
            if(range.location == 11 && range.length == 0){
                textField.text = textField.text! + "-"
            }
            if range.location == 14{
                return false
            }
        }
        if textField.tag == 2 {
            if(range.location == 2 && range.length == 0){
                textField.text = textField.text! + "."
            }
            if(range.location == 6 && range.length == 0){
                textField.text = textField.text! + "."
            }
            if(range.location == 10 && range.length == 0){
                textField.text = textField.text! + "/"
            }
            if(range.location == 15 && range.length == 0){
                textField.text = textField.text! + "-"
            }
            if range.location == 18{
                return false
            }
                    }
        return true
        
    }
}
