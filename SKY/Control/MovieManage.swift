//
//  MovieManage.swift
//  SKY
//
//  Created by marcos hayashi on 25/07/18.
//  Copyright © 2018 marcos hayashi. All rights reserved.
//

import Foundation

enum MovieError{
    
    case    URL_ERROR
    case    TASK_ERROR(error:Error)
    case    NO_RESPONDE
    case    NO_DATA
    case    RESPONSE_STATUS_CODE(code:Int)
    case    INVALID_JSON
}
class MovieManage{
    
    private static let basePath = "https://sky-exercise.herokuapp.com/api/Movies"
    
    private static let configuration: URLSessionConfiguration = {
        
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["Content-Type": "application/json"]
        config.timeoutIntervalForRequest = 30.0
        config.httpMaximumConnectionsPerHost = 5
        return config
    }()
    
    private static let session = URLSession(configuration: configuration)
    
    class func getMovies(onComplete:@escaping ([Movie]) -> Void, onError:@escaping (MovieError) -> Void){

        guard let url = URL(string: basePath) else {
            onError(.URL_ERROR)
            return
        }
        
        let dataTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error == nil {
                guard let response = response as? HTTPURLResponse else {
                    onError(.NO_RESPONDE)
                    return}
                
                if response.statusCode == 200{
                    guard let data = data else {
                        onError(.NO_DATA)
                        return}
                    do{
                        let movies = try JSONDecoder().decode([Movie].self, from: data)
                        onComplete(movies)
                        
                    } catch{
                        onError(.INVALID_JSON)
                    }
                }else{
                    onError(.RESPONSE_STATUS_CODE(code: response.statusCode))
                    print("Algo dei errado")
                }
                
            } else {
                onError(.TASK_ERROR(error: error!))
                print(error!)
            }
            
        }
        dataTask.resume()
    }
    
}
